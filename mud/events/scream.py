# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class ScreamEvent(Event3):
    NAME = "scream"

    def perform(self):
        self.add_prop("screamed-"+self.object2)
        self.inform("scream")
